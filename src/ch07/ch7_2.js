exports.my_regexp = /"(?:\\.|[^\\\""])*"/g;

exports.my_regexp_ex = new RegExp("\"(?:\\\\.|[^\\\\\\\"])*\"", 'g');

exports.make_a_matcher = function () {
	return /a/gi;
};
