// parse URI
exports.parse_url = /^(?:([A-Za-z]+):)?(\/{0,3})([0-9.\-A-Za-z]+)(?::(\d+))?(?:\/([^?#]*))?(?:\?([^#]*))?(?:#(.*))?$/;

exports.names = ['url', 'scheme', 'slash', 'host', 'port', 'path', 'query', 'hash'];

exports.blanks = ' ';

// parse number
exports.parse_number = /^-?\d+(?:\.\d*)?(?:e[+\-]?\d+)?$/i;

exports.test = function (num) {
	return exports.parse_number.test(num);
};
