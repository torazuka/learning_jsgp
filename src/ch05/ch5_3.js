exports.myMammal = {
    name : 'Herb the Mammal', 
    get_name : function () {
        return this.name;
    },
    says : function () {
        return this.saying || '';
    }
};

exports.block = function (){
    var oldScope = scope;
    scope = Object.create(scope);
    
    advance('{');
    
    parse(scope);
    
    advance('}');
    
    // 新しいscopeを破棄して古いscopeに戻す
    scope = opdscope;
};