
// specにはコンストラクタがインスタンス生成に必要な
// すべての情報が格納されている。

exports.constructor = function(spec, my) {
    // インスタンスプライベート変数
    var that;
    var n = spec.get_name();
    
    my = my || {};
    
    // 共有の変数や関数をmyに追加
    
    my.foo = function () {
        // specとmy、プライベート変数にアクセス可能
    };
    my.member = 'bar';
    
    that = Object.create(spec);
    
    // 特殊機能を持つメソッドを定義してオブジェクトの
    // インタフェースを整える
    
    // ここではメソッドの定義を2段階に分けている
    
    var methodical = function () {
        // ...
    };
    that.methodical = methodical;
    
    return that;
};




