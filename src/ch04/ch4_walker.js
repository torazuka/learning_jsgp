exports.walk_the_DOM = function walk(node, func) {
    func(node);
    node = node.firstChild;
    while(node) {
        walk(node, func);
        node = node.nextSibling;
    }
};

exports.getElementsByAttribute = function (att, value) {
    var results = [];
    
    exports.walk_the_DOM(document.body, function(node) {
        var actual = node.nodeType === 1 && node.getAttribute(att);
        if(typeof actual === 'string' && 
            (actual === value || typeof value !== 'string')) {
            results.push(node);
        }
    });
    
    return results;
};

exports.factorial = function factorial(i, a) {
    a = a || 1;
    if (i < 2) {
        return a;
    }
    return factorial(i - 1, a * i);
};