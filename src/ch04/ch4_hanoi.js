exports.hanoi = function (disc, src, aux, dst){
    if (0 < disc){
        exports.hanoi(disc - 1, src, dst, aux);
        console.log('Move disc ' + disc + ' from ' + src + ' to ' + dst);
        exports.hanoi(disc - 1, aux, src, dst);
    }
};



