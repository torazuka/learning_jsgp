exports.add = function(a, b){
    if (typeof a !== 'number' || typeof b !== 'number') {
        throw {
            name: 'TypeError',
            message: 'add needs numbers'
        };
    }
    return a + b;
};

exports.myObject = {
    value: 0, 
    increment: function (inc) {
        this.value += typeof inc ==='number' ? inc : 1;
    },
    reset: function () {
        this.value = 0;
    }
};

exports.myObject.double = function() {
    // thisの値を退避, thisはグローバルオブジェクト
    var that = this;
    
    var helper = function() {
        that.value = exports.add(that.value, that.value);
        //this.value = exports.add(this.value, this.value); // ★
    };
    
    helper();
};

exports.Quo = function (string) {
    this.status = string;
};

// get_statusをQuoのすべてのインスタンスで利用可能にする
exports.Quo.prototype.get_status = function() {
    return this.status;
};

var array = [3, 4];
exports.sum = exports.add.apply(null, array);

exports.statusObject = {
    status: 'A-OK'
};

