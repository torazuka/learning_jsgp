
// See 1.3 or 4.7
 Function.prototype.method = function(name, func){
    this.prototype[name] = func;
    return this;
 };

Array.method('reduce', function(f, value){
	var i;
	for (i = 0; i < this.length; i += 1) {
		value = f(this[i], value);
	}
	return value;
});

exports.add = function (a, b){
    return a + b;
 };

 exports.mult = function(a, b){
 	return a * b;
 };

