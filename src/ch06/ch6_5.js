exports.is_array = function (value) {
	return value && 
	typeof value === 'object' &&
	value.constructor === Array;
};

exports.is_array_ex = function (value) {
	return value &&
		typeof value === 'object' && 
		typeof value.length === 'number' &&
		typeof value.splice === 'function' &&
		!(value.propertyIsEnumerable('length'));
};

exports.out_array = [0, 1, 2];
exports.out_array.constructor = "hoge";