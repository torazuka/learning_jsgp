var assert = require('assert');
var obj = require("../../src/ch07/ch7_3.js");

describe('7.3.1 選択肢', function(){
    it('inに先にマッチして、intにはマッチしない', function(){

    	var result = "into".match(obj.s7_3_1);
        assert(result.length === 1);
    	assert(result[0] === "in");
    })
});

describe('7.3.2 正規表現シーケンス', function(){
    it('1個以上の数字列にマッチ', function(){
        var result0 = "777".match(obj.s7_3_2);
        assert(result0[0] === "777");

        var result1 = "a12b3c".match(obj.s7_3_2);
        assert(result1[0] === "12");
        assert(result1[1] === "3");
    })
});

describe('7.3.3(a) 正規表現因子', function(){
    it('エスケープしたピリオドは，ピリオド文字自体にマッチする', function(){

        var result = "java.util.Set".match(obj.s7_3_3_a);
        assert(result[0] === ".");
    })
});

describe('7.3.3(b) 正規表現因子', function(){
    it('エスケープしないピリオドは，改行文字以外のすべての文字にマッチする', function(){

        var result = "java.util.Set".match(obj.s7_3_3_b);
        assert(result[0] === "j");
    })
});

describe('7.3.4 正規表現シーケンス', function(){
    it('エスケープした10個連続の「ｗ」にマッチする', function(){

        var result= "凱旋門ｗｗｗｗｗｗｗｗｗｗ".match(obj.s7_3_4);
        assert(result[0] === "ｗｗｗｗｗｗｗｗｗｗ");
    })
});

describe('7.3.4 正規表現シーケンス', function(){
    it('単語の後にホワイトスペースが続き、その後にもう一度同じ単語が現れている部分にマッチ', function(){

        var result = "hoge moga fuga fuga piyo".match(obj.doubled_words);
        assert(result[0] === "fuga fuga");
    })
});

describe('7.3.5(a) 正規表現グループ キャプチャ', function(){
    it('マッチするクラス名をキャプチャする', function(){

        var result0 = "java.util.Set".match(obj.s7_3_5_a);
        assert(result0[0] === "Set");

        var result1 = "java.util.Serializable".match(obj.s7_3_5_a);
        assert(result1[0] === "Serializable");
    })
});

describe('7.3.5(b) 正規表現グループ 肯定先読み', function(){
    it('Windows 98のWindowsにはマッチしないが，Windows XPのWindowsにはマッチする', function(){

        var result0 = "Windows 98".match(obj.s7_3_5_b);
        assert(result0 === null);

        var result1 = "Windows XP".match(obj.s7_3_5_b);
        assert(result1 !== null);
        assert(result1[0] === "Windows ");
    })
});

describe('7.3.5(c) 正規表現グループ 否定先読み', function(){
    it('Windows VistaのWindowsにはマッチしないが，Windows LonghornのWindowsにはマッチする', function(){

        var result0 = "Windows Vista".match(obj.s7_3_5_c);
        assert(result0 === null);

        var result1 = "Windows Longhorn".match(obj.s7_3_5_c);
        assert(result1 !== null);
    })
});

describe('7.3.6 正規表現クラス', function(){
    it('アスキーの特殊文字にマッチする', function(){

        var result = "hoge@sample.com".match(obj.s7_3_6);
        assert(result.length === 2);
        assert(result[0] === "@");
        assert(result[1] === ".");
    })
});


describe('7.3.7(a) 正規表現クラスにおけるエスケープ', function(){
    it('文字クラスの先頭にエスケープしないハットマークがあると補集合を表わす', function(){

        var result0 = "AKB".match(obj.s7_3_7_a);
        assert(result0 === null);

        var result1 = "48".match(obj.s7_3_7_a);
        assert(result1 !== null);
    })
});

describe('7.3.7(b) 正規表現クラスにおけるエスケープ', function(){
    it('文字クラスの中でエスケープされたハットマークは，ハットマーク文字にマッチする', function(){

        var result0 = "48".match(obj.s7_3_7_b);
        assert(result0 === null);

        var result0 = "48^".match(obj.s7_3_7_b);
        assert(result0 !== null);
    })
});

describe('7.3.8(a) 正規表現シーケンス', function(){
    it('2～3個連続する「A」にマッチ', function(){

        var result = "oAooAoAAoooAAAoo".match(obj.s7_3_8_a);
        assert(result.length === 2);
        assert(result[0] === "AA");
        assert(result[1] === "AA");
    })
});

describe('7.3.8(b) 正規表現シーケンス', function(){
    it('2～3個連続する「A」に複数回マッチ', function(){

        var result = "oAooAoAAoooAAAoo".match(obj.s7_3_8_b);
        assert(result.length === 2);
        assert(result[0] === "AA");
        assert(result[1] === "AAA");
    })
});