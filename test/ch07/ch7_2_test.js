var assert = require('assert');
var obj = require("../../src/ch07/ch7_2.js");

describe('7.2. 正規表現オブジェクト', function(){
    it('JavaScript文字列にマッチする正規表現オブジェクトを生成する（正規表現リテラル）', function(){
    	var result = obj.my_regexp.exec('"hoge"');

    	assert(result[0], '"hoge"');
    })
});

describe('7.2. 正規表現オブジェクト', function(){
    it('JavaScript文字列にマッチする正規表現オブジェクトを生成する（RegExpコンストラクタ関数）', function(){
    	var result = obj.my_regexp_ex.exec('"moga"');

    	assert(result[0], '"moga"');
    })
});

describe('7.2. RegExpオブジェクト', function(){
    it('同じ正規表現リテラルで生成されたRegExpオブジェクトは、単一のインスタンスを共有する', function(){
    	var x = obj.make_a_matcher;
    	var y = obj.make_a_matcher;

    	x.lastIndex = 10;

    	assert(y.lastIndex === 10);
    })
});
