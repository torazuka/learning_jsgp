var assert = require('assert');
var obj = require("../../src/ch07/ch7_1.js");

describe('7.1. 正規表現の例', function(){
    it('URIをパースする', function(){

        var result = obj.parse_url.exec("http://www.ora.com:80/goodparts?q#fragment");

        var result_set = Array(8);
        var i;
        for (i = 0; i < obj.names.length; i++) {

            // 空白に適用されるsubstringは、項目名の文字数分だけ空白を切り取る
            result_set[i] = obj.names[i] + ':' + obj.blanks.substring(obj.names[i].length), result[i];
        };

        assert(result_set[0] === 'url' + ':' + ' '.substring('url'.length), result[0]);
        assert(result_set[7] === 'hash' + ':' + ' '.substring('hash'.length), result[7]);
    })
});

describe('7.1. 正規表現の例', function(){
    it('数をパースする', function(){

        var test = obj.test;

        assert(test('1') === true);
        assert(test('number') === false);
        assert(test(98.6) === true);
        assert(test('132.21.86.100') === false);
        assert(test('123.45E-67') === true);
        assert(test('123.45e-67') === true);
        assert(test('123.45D-67') === false);
        // assert(test('98.') === false);
    })
});