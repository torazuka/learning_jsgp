var assert = require('assert');
var obj = require("../../src/ch06/ch6_5.js");

describe('prototype', function(){
    it('6.5 check array by bad know-how 1', function(){
        var myArray = [0, 1, 2];
        assert(obj.is_array(myArray) === true);
    })
});


describe('prototype', function(){
    it('6.5 check array by bad know-how 2', function(){
        var myArray = obj.out_array;
        assert(obj.is_array(myArray) === false);

        assert(obj.is_array_ex(myArray) === true);
    })
});

describe('prototype', function(){
    it('6.5 check array with Array#isArray', function(){
        var myArray = obj.out_array;
        assert(Array.isArray(myArray) === true);
    })
});