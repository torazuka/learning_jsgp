var assert = require('assert');
var obj = require("../../src/ch06/ch6_2.js");

describe('prototype', function(){
    it('6.2. 配列の長さ', function(){
        var myArray = obj.myArray;
        assert(myArray.length === 0);

		myArray[1000000] = true;
        assert(myArray.length === 1000001);

        var numbers = obj.numbers;
        assert(numbers.length === 3);
        numbers[numbers.length] = 'shi';
        assert(numbers.length === 4);
        assert(numbers[numbers.length - 1] = 'shi');

        numbers.push('go');
        assert(numbers.length === 5);
        assert(numbers[numbers.length - 1] === 'go');
    })
});
