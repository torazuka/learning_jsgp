var assert = require('assert');
var obj = require("../../src/ch06/ch6_1.js");

describe('prototype', function(){
    it('6.1. 配列リテラル', function(){
        var empty = obj.empty;
        assert(empty[1] === undefined);

        var numbers = obj.numbers;
        assert(numbers[1] === 'one');

        assert(empty.length === 0);
        assert(numbers.length === 10);

        var numbers_object = obj.numbers_object;
        assert(numbers_object.length === undefined);

        var misc = obj.misc;
        assert(misc.length === 10);
        assert(misc[6][0] === 'nested');
    })
});

