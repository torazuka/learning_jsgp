var assert = require('assert');
var obj = require("../../src/ch06/ch6_4.js");

describe('prototype', function(){
    it('6.4. 要素の列挙', function(){
        var myArray = obj.myArray;
        var str = "";
        for(var i = 0; i < myArray.length; i++){
        	str += myArray[i];
        }
        assert(str === "zeroonetwoshigo");
    })
});