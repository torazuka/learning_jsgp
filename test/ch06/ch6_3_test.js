var assert = require('assert');
var obj = require("../../src/ch06/ch6_3.js");

describe('prototype', function(){
    it('6.3. 要素の削除(delete)', function(){
        var numbers = obj.numbers;
        assert(numbers.length === 5);
        assert(numbers[2] === 'two');

        delete numbers[2];
        assert(numbers.length === 5);
        assert(numbers[2] === undefined);
    })
});

describe('prototype', function(){
    it('6.3. 要素の削除(splice)', function(){
        // var numbers = obj.numbers: // this is NG because numbers has changed!
        var numbers = obj.original_numbers;
        assert(numbers.length === 5);
        assert(numbers[2] === 'two');

        numbers.splice(2, 1);
        assert(numbers.length === 4);
        assert(numbers[2] === 'shi');
    })
});