var assert = require('assert');
var obj = require("../../src/ch06/ch6_6.js");

describe('prototype', function(){
    it('6.6 method of array', function(){
    	var data = [4, 8, 15, 16, 23, 42];

    	var add = obj.add;
    	var sum = data.reduce(add, 0);
    	
    	// for recalculation
    	var expected = 0;
    	var i;
    	for(i = 0; i < data.length; i += 1){
    		expected += data[i];
    	}
        assert(sum === expected); // sum === 108
    })
});

describe('prototype', function(){
    it('6.6 method of array', function(){
    	var data = [4, 8, 15, 16, 23, 42];

    	var mult = obj.mult;
    	var sum = data.reduce(mult, 0);
    	
    	// for recalculation
    	var expected = 0;
    	var i;
    	for(i = 0; i < data.length; i += 1){
    		expected *= data[i];
    	}
        assert(sum === expected); // sum === 7418880
    })
});

describe('prototype', function(){
    it('6.6 method of array', function(){
    	var data = [4, 8, 15, 16, 23, 42];

    	var add = obj.add;
    	data.total = function () {
    		return this.reduce(add, 0);
    	};
    	var total = data.total();
    	
    	// for recalculation
    	var expected = 0;
    	var i;
    	for(i = 0; i < data.length; i += 1){
    		expected += data[i];
    	}
        assert(total === expected); // total === 108
    })
});


describe('prototype', function(){
    it('6.6 配列のlength番目の要素に値を代入すると、lengthがインクリメントされる。', function(){
    	var data = [4, 8, 15, 16, 23, 42];

    	assert(data.length === 6);
    	data[data.length] = 1;
    	assert(data.length === 7);
    })
});

describe('prototype', function(){
    it('6.6 オブジェクトの（変数）lengthという名前のプロパティに値を代入しても、lengthの値はインクリメントされない。', function(){
    	var data = [4, 8, 15, 16, 23, 42];

    	// a value which is got from Object.create is just a object
    	var foo = Object.create(data);
    	
    	assert(foo.length === 6);
    	foo[foo.length] = 1;

    	// This is NOT Array.length
    	assert(foo.length === 6);
    	
    	// 6 is name of property
    	assert(foo[6] === 1);
    })
});

describe('prototype', function(){
    it('6.6 配列に値をpushすると、lengthがインクリメントされる。', function(){
    	var data = [4, 8, 15, 16, 23, 42];

    	assert(data.length === 6);
    	data.push(1);
    	assert(data.length === 7);
    })
});

describe('prototype', function(){
    it('6.6 オブジェクトに値をpushすると、lengthがインクリメントされる？？', function(){
    	var data = [4, 8, 15, 16, 23, 42];

    	// a value which is got from Object.create is just a object
    	var foo = Object.create(data);
    	
    	assert(foo.length === 6);
    	foo.push(1);

    	// This is NOT Array.length
    	assert(foo.length === 7);
    })
});