var assert = require('assert');
var obj = require("../../src/ch04/ch4");

describe('Function', function(){
    it('4.2. should return result of addition', function(){
        assert(obj.add(1, 2) === 3);
    })
});

describe('Function', function(){
    it('4.3.1. should invoke a method', function(){
    
        assert(obj.myObject.value === 0);
        
        obj.myObject.increment();
        assert(obj.myObject.value === 1);
        
        obj.myObject.increment(2);
        assert(obj.myObject.value === 3);
        
        obj.myObject.increment('a'); // This is not a number.
        assert(obj.myObject.value === 4);
    })
});

describe('Function', function(){
    it('4.3.2. should invoke a function', function(){
        obj.myObject.reset();
        assert(obj.myObject.value === 0);
        
        obj.myObject.increment(5);
        assert(obj.myObject.value === 5);
        obj.myObject.double();
        assert(obj.myObject.value === 10);
    })
});

describe('Function', function(){
    it('4.3.3. should invoke a constructor', function(){
        var myQuo = new obj.Quo("confused");
        assert(myQuo.get_status() === "confused");
    })
});

describe('Function', function(){
    it('4.3.4. should return result with apply', function(){
        assert(obj.sum === 7);
        
        // よくわからない
        var status = obj.Quo.prototype.get_status.apply(obj.statusObject);
        assert(status === "A-OK");
    })
});

describe('Function', function(){
    it('4.4. should return 108', function(){
        var sum = function () {
            var i, sum = 0;
             for(i = 0; i < arguments.length; i += 1){
                sum += arguments[i];
            }
            return sum;
        };
        assert(sum(4, 8, 15, 16, 23, 42) === 108);
    })
});

describe('Function', function(){
    it('4.6. should return TypeError', function(){
        var foo = {};
        var try_it = function (){
            try {
                obj.add("seven");
            } catch(e) {
                foo.log = e.name + ": " + e.message;
            }
        }
        
        try_it();
        assert(foo.log === "TypeError: add needs numbers");
    })
});

describe('Function', function(){
    it('4.7. expand prototype', function(){
        Function.prototype.method = function (name, func) {
            if(this.prototype[name] === undefined){
                this.prototype[name] = func;
                return this;
            }
        };
        
        Number.method('integer', function() {
            // 整数部分だけを取り出す。ceilは負数、floorは正数用。
            return Math[this < 0 ? 'ceil' : 'floor'](this);
        });
        
        assert((-10 / 3).integer() === -3);
        
        String.method('trim', function(){
            return this.replace(/^\s+|\s+$/g, '');
        });
        
        assert('"' + " neat ".trim() + '"', '"neat"');
    })
});