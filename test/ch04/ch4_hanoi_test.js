var assert = require('assert');
var hanoi = require("../../src/ch04/ch4_hanoi");
var walker = require("../../src/ch04/ch4_walker");

describe('Function', function(){
    it('4.8. should return result of hanoi', function(){
        hanoi.hanoi(3, 'Src', 'Aux', 'Dst');
    })
});

describe('Function', function(){
    it('4.8. should return 24', function(){
        assert(walker.factorial(4) === 24);
    })
});
