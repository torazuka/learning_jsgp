var assert = require('assert');
var obj = require("../../src/ch05/ch5_3.js");

describe('prototype', function(){
    it('5.3. should return "meow Henrietta meow"', function(){
        var myCat = Object.create(obj.myMammal);
        
        myCat.name = 'Henrietta';
        myCat.saying = 'meow';
        myCat.purr = function (n) {
            var i, s = '';
            for(i = 0; i < n; i += 1){
                if (s) {
                    s += '-';
                }
                s += 'r';
            }
            return s;
        };
        myCat.get_name = function () {
            return this.says() + ' ' + this.name + ' ' + this.says();
        };
        
        assert(myCat.get_name() === "meow Henrietta meow");
    })
});

