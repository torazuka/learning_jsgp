var assert = require('assert');
var obj = require("../../src/ch05/ch5_4_mammal.js");

describe('prototype', function(){
    it('5.4. should return "Herb"', function(){
        var myMammal = obj.mammal({name: 'Herb'});
        
        var myCat = Object.create(myMammal);
        assert(myCat.get_name() === "Herb");
    })
});

describe('prototype', function(){
    it('5.4. should return "meow Henrietta meow"', function(){
        var myCat = obj.cat({name: 'Henrietta'});
        
        assert(myCat.get_name() === "meow Henrietta meow");
    })
});
