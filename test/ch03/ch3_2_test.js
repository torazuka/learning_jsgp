var assert = require('assert');
var expect = require('expect.js');
var obj = require("../../src/ch03/ch3");

describe('Object', function(){
    it('3.2. should return undefined', function(){
        var flight = obj.Flight;
        
        assert(flight.foo === undefined);
    })
});

describe('Object', function(){
    it('3.2. should return initial value', function(){
        var stooge = obj.Stooge;
        var middle = stooge["middle-name"] || "(none)";
        
        assert(middle === "(none)");
    })
});

describe('Object', function(){
    it('3.2. should return initial value', function(){
        var flight = obj.Flight;
        var status = flight.stats || "unknown";
        
        assert(status === "unknown");
    })
});

describe('Object', function(){
    it('should return undefined', function(){
        var flight = obj.Flight;
        
        assert(flight.equipment === undefined);
    })
});

describe('Object', function(){
    it('should throw Error', function(){
        var flight = obj.Flight;
        
        expect( function(){
            flight.equipment.model;
        } ).to.throwException();
    })
});

describe('Object', function(){
    it('should avoid Error', function(){
        var flight = obj.Flight;
        
        assert((flight.equipment && flight.equipment.model) === undefined); // note operator precedence
    })
});