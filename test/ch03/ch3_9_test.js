var assert = require('assert');
var obj = require("../../src/ch03/ch3");

describe('global value', function(){
    it('3.9. should return global value', function(){
        var MYAPP = {};
        
        MYAPP.stooge = obj.Stooge;
        
        MYAPP.flight = obj.Flight;
        
        assert(MYAPP.stooge["first-name"] === "Jarome");
        assert(MYAPP.flight.arrival.city === "Los Angels");
    })
});
