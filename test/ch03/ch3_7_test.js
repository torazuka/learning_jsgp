var assert = require('assert');
var obj = require("../../src/ch03/ch3");
var _ = require("../../lib/underscore-min.js");

describe('Object', function(){
    it('3.7. should return properties', function(){
        var stooge = obj.Stooge;
        if(typeof Object.create !== 'function') {
            Object.create = function (o) {
                var F = function () {};
                F.prototype = o;
                return new F();
            };
        }
        var another_stooge = Object.create(stooge);
        another_stooge['first-name'] = "Harry";
        another_stooge['middle-name'] = "Moses";
        another_stooge.nickname = "Moe";
        stooge.profession = "actor";

        var foo = {}; // need init
        
        // functionでないプロパティをfooにコピーする
        var name;
        for (name in another_stooge) {
            if(typeof another_stooge[name] !== 'function'){
                foo[name] = another_stooge[name];
            }
        }
        
        assert(foo["first-name"] === "Harry");
        assert(foo["middle-name"] === "Moses");
        assert(foo["nickname"] === "Moe");
        assert(foo["last-name"] === "Howard");
        assert(foo["profession"] === "actor");
    })
});

describe('_.reduce', function(){
    it('3.7. should return properties', function(){
        var stooge = obj.Stooge;
        if(typeof Object.create !== 'function') {
            Object.create = function (o) {
                var F = function () {};
                F.prototype = o;
                return new F();
            };
        }
        var another_stooge = Object.create(stooge);
        another_stooge['first-name'] = "Harry";
        another_stooge['middle-name'] = "Moses";
        another_stooge.nickname = "Moe";
        stooge.profession = "actor";
 
        var foo = _.reduce(another_stooge, function(m, n, i) {
            m[i] = n;
            return m;
        }, []);
        
        assert(foo["first-name"] === "Harry");
        assert(foo["middle-name"] === "Moses");
        assert(foo["nickname"] === "Moe");
        assert(foo["last-name"] === undefined);
        assert(foo["profession"] === undefined);
    })
});

describe('Object', function(){
    it('3.7. should return properties', function(){
        var stooge = obj.Stooge;
        if(typeof Object.create !== 'function') {
            Object.create = function (o) {
                var F = function () {};
                F.prototype = o;
                return new F();
            };
        }
        var another_stooge = Object.create(stooge);
        another_stooge['first-name'] = "Harry";
        another_stooge['middle-name'] = "Moses";
        another_stooge.nickname = "Moe";
        stooge.profession = "actor";

        var foo = {}; // need init
        var i;
        var properties = [
            "first-name",
            "middle-name",
            "last-name",
            "profession"
        ];
        for (i = 0; i < properties.length; i++){
            foo[properties[i]] = another_stooge[properties[i]];
        }
        
        assert(foo[properties[0]] === "Harry");
        assert(foo[properties[1]] === "Moses");
        assert(foo[properties[2]] === "Howard");
        assert(foo[properties[3]] === "actor");
    })
});

