var assert = require('assert');
var obj = require("../../src/ch03/ch3");

describe('empty', function(){
    it('3.1. should return undefined', function(){
        var empty = obj.Empty_object;
        
        assert(empty['foo'] === undefined);
    })
});

describe('Object', function(){
    it('3.1. should return value by name', function(){
        var stooge = obj.Stooge;
        
        assert(stooge['first-name'] === "Jarome");
        assert(stooge['last-name'] === "Howard");
    })
});

describe('Object', function(){
    it('3.1. should return value by name', function(){
        var flight = obj.Flight;
        
        assert(flight['airline'] === "Oceanic");
        assert(flight.departure.IATA === "SYD");
    })
});
