var assert = require('assert');
var obj = require("../../src/ch03/ch3");

describe('Object', function(){
    it('3.8. should delete another_stooge.nickname', function(){
        var stooge = obj.Stooge;
        if(typeof Object.create !== 'function') {
            Object.create = function (o) {
                var F = function () {};
                F.prototype = o;
                return new F();
            };
        }
        var another_stooge = Object.create(stooge);
        another_stooge.nickname = "Moe";
        stooge.nickname = "Curly";
        assert(another_stooge.nickname === "Moe");
        
        delete another_stooge.nickname;
        assert(another_stooge.nickname === "Curly");
    })
});
