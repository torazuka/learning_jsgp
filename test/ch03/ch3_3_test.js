var assert = require('assert');
var expect = require('expect.js');
var obj = require("../../src/ch03/ch3");

describe('Object', function(){
    it('3.3. should return updated value', function(){
        var stooge = obj.Stooge;
        stooge['first-name'] = "foo";
        assert(stooge['first-name'] === "foo");
        
        stooge['middle-name'] = "Lester";
        assert(stooge['middle-name'] === "Lester");
        
        stooge.nickname = "Curly";
        assert(stooge.nickname === "Curly");
    })
});

describe('Object', function(){
    it('3.3. should return updated value', function(){
        var flight = obj.Flight;
        flight.equipment = {
            model: 'Boeing 777'
        };
        assert(flight.equipment.model === "Boeing 777");
        
        flight.status = 'overdue';
        assert(flight.status === "overdue");
    })
});
