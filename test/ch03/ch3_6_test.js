var assert = require('assert');
var obj = require("../../src/ch03/ch3");

describe('Object', function(){
    it('3.6. should return reflect value', function(){
        var flight = obj.Flight;
        flight.equipment = {
            model: 'Boeing 777'
        };
        flight.status = 'overdue';
        
        assert(typeof flight.number === "number");
        assert(typeof flight.status === "string");
        assert(typeof flight.arrival === "object");
        assert(typeof flight.manifest === "undefined"); // not undefined. This is string.
        
        assert(typeof flight.toString === "function");
        assert(typeof flight.constructor === "function");
        
        assert(flight.hasOwnProperty('number') === true);
        assert(flight.hasOwnProperty('constructor') === false);
    })
});
