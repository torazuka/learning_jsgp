var assert = require('assert');
var obj = require("../../src/ch03/ch3");

describe('Object', function(){
    it('3.4. should return reference value', function(){
        var stooge = obj.Stooge;
        var x = stooge;
        x.nickname = "Curly";
        var nick = stooge.nickname;
        assert(nick === "Curly");
    })
});

describe('Object', function(){
    it('3.4. should return reference value', function(){
        var a = {}, b = {}, c = {};
        assert((a === b) === false);
        
        a = b = c = {};
        assert((a === b) === true);
    })
});
